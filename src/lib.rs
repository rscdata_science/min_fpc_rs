use eotrs::{BlockSize, MultiRasterDatasetBuilder, SingleRasterDatasetBuilder};

use ndarray::Zip;
use ndarray::{s, Array2, Array3, Array4, Axis};
use ndarray_stats::Sort1dExt;
use rayon;
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;

mod tests;

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &Array3<i32>) -> Array2<i32> {
    let red = &reflectance_data
        .slice(s![0, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1, .., ..])
        .mapv(|elem| elem as f32);
    let ndvi = ((nir - red) / (nir + red)).mapv(|elem| (elem * 10000.) as i32);
    ndvi
}

// For a Array4 with time in the first axis, and red in the 1st index of the second
// axis, and nir in the second index of the second axis, it will compute
// ndvi for each time step.
fn ndvi_over_time(reflectance_data: &Array4<i32>) -> Array3<i32> {
    let r = reflectance_data.shape()[3];
    let c = reflectance_data.shape()[2];
    let t = reflectance_data.shape()[0];
    let mut result = Array3::zeros((t, r, c));
    let mut idx = 0;
    for ref_t in reflectance_data.axis_iter(Axis(0)) {
        let ref_t = ref_t.to_owned();
        let ndvi_t = ndvi(&ref_t);
        result.slice_mut(s![idx, .., ..]).assign(&ndvi_t);
        idx += 1;
    }
    result
}

fn compute_fpc(ndvi: i32) -> u8 {
    let coef: Vec<f64> = vec![-6.9809513, 65.73058, 51.320786];
    (coef[0] +
    (ndvi as f64 / 10000.) * coef[1] +
    (ndvi as f64 / 10000.) * (ndvi as f64 / 10000.) * coef[2]) as u8

}

// Compute the fpc for a block. It will first compute the ndvi for
// each time step, then find the second smallest ndvi in the time series
// and finally apply a 2nd order transformation to the ndvi.
// It also computes the uncertainity bounds.
// Will return an Array3 with fpc,min_bound,max_bound in the first
// axis and rows and cols in the 2nd and 3rd.
fn bloc_worker(data: Array4<i32>) -> Array2<u8> {
    // define requiered coefficents.
    let bounds_low: Vec<f64> = vec![
        -6.0, -12.5, -17.4, -17.1, -40.0, -29.1, -45.0, -19.3, -12.7, -2.2,
    ];
    let bounds_high: Vec<f64> = vec![8.7, 15.2, 25.2, 27.3, 24.4, 18.0, 20.6, 12.9, 10.4, 6.3];

    let nt = data.shape()[0];
    let nr = data.shape()[2];
    let nc = data.shape()[3];

    // compute the ndvi for each time step
    println!("data {:?}\n", data);

    let ndvi = ndvi_over_time(&data);

    // reshape to have a time slice per row and have acces to get_from_sorted.
    let ndvi_reshape = ndvi.t().into_shape((nr * nc, nt)).unwrap();
    // find the second smallest ndvi
    println!("ndvi_reshape {:?}\n", ndvi_reshape);
    

    let second_smallest_ndvi = Zip::from(ndvi_reshape.rows())
        .map_collect(|time_slice| time_slice.to_owned().view_mut().get_from_sorted_mut(2))
        .into_shape((nr, nc))
        .unwrap();
    
    println!("second_smallest_ndvi {:?}\n", second_smallest_ndvi);
    let fpc = second_smallest_ndvi.mapv(|v| {
        compute_fpc(v)
    });
    println!("fpc {:?}\n", fpc);

    let num_bins = 9.;
    let fpc_bin = fpc.mapv(|v| (v / num_bins) as usize);

    let low_uncertainty_bound = Zip::from(&fpc)
        .and(&fpc_bin)
        .map_collect(|fpc, bin| fpc + bounds_low[*bin]);

    let high_uncertainty_bound = Zip::from(&fpc)
        .and(&fpc_bin)
        .map_collect(|fpc, bin| fpc + bounds_high[*bin]);

    fpc
}

// process the input files and save the fpc per block to disk.
pub fn process(input_files: Vec<&str>, n_threads: usize, output_file: &str) {
    let mut srds_list = Vec::new();
    for source in input_files {
        let srds = SingleRasterDatasetBuilder::from_file(source)
            .bands(vec![2, 3]) // red,nir -> 0,1
            .block_size(BlockSize {
                rows: 1024,
                cols: 1024,
            })
            .build();
        srds_list.push(srds);
    }
    let mrds = MultiRasterDatasetBuilder::from_datasets(srds_list).build();

    let pool = rayon::ThreadPoolBuilder::new()
        .num_threads(n_threads)
        .build()
        .unwrap();

    pool.install(|| {
        mrds.blocks_attributes
            .to_owned()
            .into_par_iter()
            .for_each(|block_attributes| {
                let w = block_attributes.read_window;
                let data = mrds.read_window::<i32>(w);
                let fpc = bloc_worker(data);
                mrds.write_window(format!("fpc_{}.tif",block_attributes.block_index))
            })
    })
}
