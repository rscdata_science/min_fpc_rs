#[allow(unused_imports)]
use rayon::vec;
extern crate glob;
use self::glob::glob;
use super::*;
use gdal::Dataset;
use ndarray::prelude::*;
use ndarray::Zip;
use ndarray_stats::Sort1dExt;
use std::path::Path;

#[test]
fn test_ndvi() {
    let red = vec![3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000];
    let nir = vec![7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000];

    let mut test_data = Vec::new();
    test_data.extend(red);
    test_data.extend(nir);
    let test_data = Array3::from_shape_vec((2, 3, 3), test_data).unwrap();
    let ndvi = ndvi(&test_data);
    let expected = vec![4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000];
    let expected_array = Array2::from_shape_vec((3, 3), expected).unwrap();
    assert_eq!(expected_array, ndvi);
}

#[test]
fn test_ndvi_time() {
    let data = vec![3000, 7000, 4000, 8000];

    let test_data = Array4::from_shape_vec((2, 2, 1, 1), data).unwrap();
    let ndvi = ndvi_over_time(&test_data);
    let expected = Array3::from_shape_vec((2, 1, 1), vec![4000, 3333]).unwrap();
    assert_eq!(expected, ndvi);
}

#[test]
fn test_block_worker() {
    // load test data in:
    let input_files: Vec<_> = glob("test_data/*abm*.img").unwrap().collect();
    let mut srds_list = Vec::new();
    // Create the MultiRasterDataset
    for source in &input_files {
        let s = source.as_ref().unwrap().to_str().unwrap();
        let srds = SingleRasterDatasetBuilder::from_file(s)
            .bands(vec![2, 3]) // red,nir -> 0,1
            .block_size(BlockSize { rows: 5, cols: 5 })
            .build();
        srds_list.push(srds);
    }
    let mrds = MultiRasterDatasetBuilder::from_datasets(srds_list).build();

    // get a block
    let read_window = mrds.blocks_attributes[0].read_window;
    let data = mrds.read_window::<i32>(read_window);
    // compute the fpc
    let fpc = bloc_worker(data);
    // get the expected values
    let expeted_srds =
        SingleRasterDatasetBuilder::from_file("test_data/cvmsre_sub_y20162017_acta2.img")
            .bands(vec![1])
            .block_size(BlockSize { rows: 5, cols: 6 })
            .build();
    let expected_data = expeted_srds.read_window::<i32>(read_window);
    let eq = Zip::from(&fpc)
        .and(&expected_data)
        .map_collect(|f, e| f == e);
    assert!(eq.iter().all());
}

#[test]
fn test_open() {
    let rp = "test_data/cvmsre_sub_m201606201608_abma2.img";
    let ds = Dataset::open(Path::new(&rp)).unwrap();
    let band = ds.rasterband(3).unwrap();
    let mut band_data = band
        .read_as::<i32>((0, 0), (30, 30), (25, 25), None)
        .unwrap()
        .data;
    println!("{:?}", band_data);
}
