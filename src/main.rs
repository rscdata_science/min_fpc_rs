use clap::{load_yaml, App};
use env_logger;
use log::{debug};

use min_fpc::{process};

fn main() {
    // init the logger
    let env = env_logger::Env::default().filter_or("LOG_LEVEL", "info");
    env_logger::init_from_env(env);
    debug!("Starting min_fpc");
    
    // parse arguments
    let yaml = load_yaml!("cli.yaml");
    let m = App::from(yaml).get_matches();
    let input_files:Vec<&str>  = m.value_of("input_files")
                        .unwrap()
                        .split(",")
                        .map(|x| x)
                        .collect();
    let out_file = m.value_of("output_file").unwrap();
    let n_threads: usize = m.value_of("n_threads")
        .unwrap()
        .parse()
        .expect("error parsing n_threads");
        
    process(input_files, n_threads, out_file);

}
