
#,Steps to produce 20162018 min_fpc

# Required images

```bash 
qv_recall cvmsre_qld_m201606201608_abma2.tif cvmsre_qld_m201609201611_abma2.tif cvmsre_qld_m201612201702_abma2.tif cvmsre_qld_m201703201705_abma2.tif cvmsre_qld_m201706201708_abma2.tif cvmsre_qld_m201709201711_abma2.tif cvmsre_qld_m201712201802_abma2.tif cvmsre_qld_m201803201805_abma2.tif
```

To execute locally, if cargo is available:
```bash
cargo run -- -i cvmsre_qld_m201606201608_abma2.tif,cvmsre_qld_m201609201611_abma2.tif,cvmsre_qld_m201612201702_abma2.tif,cvmsre_qld_m201703201705_abma2.tif,cvmsre_qld_m201706201708_abma2.tif,cvmsre_qld_m201709201711_abma2.tif,cvmsre_qld_m201712201802_abma2.tif,cvmsre_qld_m201803201805_abma2.tif --num_threads 24 -o min_fpc20162018.tif
```
A container will be available soon.
